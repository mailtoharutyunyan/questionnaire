package am.egs.questionnaire.app.entity.user.enums;

public enum Status {
    ACTIVE,
    NOT_ACTIVE,
    DELETED
}