package am.egs.questionnaire.app.entity.user.enums;

public enum UserRoleName {
    ROLE_USER,
    ROLE_ADMIN
}
