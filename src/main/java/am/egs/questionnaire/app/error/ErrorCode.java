package am.egs.questionnaire.app.error;

import org.springframework.http.HttpStatus;

public interface ErrorCode {

    String getName();

    HttpStatus getStatus();

}
