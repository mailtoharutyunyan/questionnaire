package am.egs.questionnaire.app.repository;

import am.egs.questionnaire.app.entity.user.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, Long> {
    User findByUsername(String username);

}

