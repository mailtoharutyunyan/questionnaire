package am.egs.questionnaire.app.repository;

import am.egs.questionnaire.app.entity.user.Authority;
import am.egs.questionnaire.app.entity.user.enums.UserRoleName;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AuthorityRepository extends JpaRepository<Authority, Long> {
    Authority findByName(UserRoleName name);
}
