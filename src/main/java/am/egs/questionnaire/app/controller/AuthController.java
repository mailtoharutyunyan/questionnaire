package am.egs.questionnaire.app.controller;

import am.egs.questionnaire.app.dto.PasswordChanger;
import am.egs.questionnaire.app.entity.user.UserTokenState;
import am.egs.questionnaire.app.response.SuccessResponse;
import am.egs.questionnaire.app.security.TokenHelper;
import am.egs.questionnaire.app.service.UserService;
import am.egs.questionnaire.app.service.impl.CustomUserDetailsService;
import am.egs.questionnaire.app.validator.RequestFieldsValidator;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/auth", produces = MediaType.APPLICATION_JSON_VALUE)
public class AuthController {

    private final RequestFieldsValidator requestFieldsValidator;
    private final CustomUserDetailsService userDetailsService;
    private final TokenHelper tokenHelper;
    private final UserService userService;

    @Value(value = "${jwt.expires_in}")
    private int EXPIRES_IN;

    @Value("${jwt.cookie}")
    private String TOKEN_COOKIE;

    @RequestMapping(value = "/refresh", method = RequestMethod.GET)
    public ResponseEntity<SuccessResponse> refreshAuthenticationToken(HttpServletRequest request, HttpServletResponse response) {

        String authToken = tokenHelper.getToken(request);
        if (authToken != null && tokenHelper.canTokenBeRefreshed(authToken)) {
            // TODO check user password last update
            String refreshedToken = tokenHelper.refreshToken(authToken);

            Cookie authCookie = new Cookie(TOKEN_COOKIE, (refreshedToken));
            authCookie.setPath("/");
            authCookie.setHttpOnly(true);
            authCookie.setMaxAge(EXPIRES_IN);
            // Add cookie to response
            response.addCookie(authCookie);

            UserTokenState userTokenState = new UserTokenState(refreshedToken, EXPIRES_IN);
            return new ResponseEntity<>(new SuccessResponse(true, userTokenState.getAccess_token()), HttpStatus.OK);
        } else {
            UserTokenState userTokenState = new UserTokenState();
            return new ResponseEntity<>(new SuccessResponse(true, userTokenState.getAccess_token()), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "/changePassword", method = RequestMethod.POST)
    @PreAuthorize("hasRole('USER')")
    public ResponseEntity<SuccessResponse> changePassword(@Valid @RequestBody PasswordChanger passwordChanger, Errors errors) {
        requestFieldsValidator.validate(errors);
        userDetailsService.changePassword(passwordChanger.getOldPassword(), passwordChanger.getNewPassword());
        return ResponseEntity.accepted().body(new SuccessResponse(true, "password successfully changed"));
    }

}
