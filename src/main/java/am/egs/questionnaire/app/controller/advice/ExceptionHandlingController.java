package am.egs.questionnaire.app.controller.advice;

import am.egs.questionnaire.app.error.ErrorResponseDto;
import am.egs.questionnaire.app.exception.BaseException;
import am.egs.questionnaire.app.exception.ExceptionResponse;
import am.egs.questionnaire.app.exception.ResourceConflictException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class ExceptionHandlingController {

    @ExceptionHandler(ResourceConflictException.class)
    public ResponseEntity<ExceptionResponse> resourceConflict(ResourceConflictException ex) {
        ExceptionResponse response = new ExceptionResponse();
        response.setErrorCode("Conflict");
        response.setErrorMessage(ex.getMessage());
        return new ResponseEntity<>(response, HttpStatus.CONFLICT);
    }

    @ExceptionHandler(BaseException.class)
    public ResponseEntity<ErrorResponseDto> handleBaseException(BaseException ex) {
        return new ResponseEntity<>(new ErrorResponseDto(
                ex.getErrorCode().getName(),
                ex.getErrors()),
                ex.getErrorCode().getStatus());
    }

}
