package am.egs.questionnaire.app.controller;

import am.egs.questionnaire.app.dto.LoginDto;
import am.egs.questionnaire.app.dto.RegisterDto;
import am.egs.questionnaire.app.dto.RegisteredUser;
import am.egs.questionnaire.app.dto.UserWithToken;
import am.egs.questionnaire.app.response.SuccessResponse;
import am.egs.questionnaire.app.security.TokenHelper;
import am.egs.questionnaire.app.service.UserService;
import am.egs.questionnaire.app.service.impl.CustomUserDetailsService;
import am.egs.questionnaire.app.validator.RequestFieldsValidator;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import javax.validation.Valid;

import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

@RestController
@RequestMapping(value = "/api/user", produces = MediaType.APPLICATION_JSON_VALUE)
@RequiredArgsConstructor
public class UserController {

    private final RequestFieldsValidator requestFieldsValidator;
    private final CustomUserDetailsService userDetailsService;
    private final TokenHelper tokenHelper;
    private final UserService userService;

    @Value(value = "${jwt.expires_in}")
    private int EXPIRES_IN;

    @Value("${jwt.cookie}")
    private String TOKEN_COOKIE;

    @RequestMapping(method = POST, value = "/signup")
    public ResponseEntity<SuccessResponse> register(@Valid @RequestBody RegisterDto userRequest,
                                                    UriComponentsBuilder ucBuilder, Errors errors) {
        requestFieldsValidator.validate(errors);
        RegisteredUser user = userService.registerNewUser(userRequest);
        new HttpHeaders().setLocation(ucBuilder.path("/api/user/{userId}").buildAndExpand(user.getId()).toUri());
        return new ResponseEntity<>(new SuccessResponse<>(true, "Registration successfully", user), HttpStatus.CREATED);
    }

    @RequestMapping(method = POST, value = "/signin")
    public ResponseEntity<SuccessResponse> login(@Valid @RequestBody LoginDto userRequest, Errors errors) {
        requestFieldsValidator.validate(errors);
        UserWithToken user = userService.login(userRequest);
        return new ResponseEntity<>(new SuccessResponse<>(true, "Login successfully", user), HttpStatus.OK);
    }

    @RequestMapping(method = GET, value = "/reset-credentials")
    public ResponseEntity<SuccessResponse> resetCredentials() {
        userService.resetCredentials();
        return ResponseEntity.accepted().body(new SuccessResponse(true,"Reset success"));
    }

}
