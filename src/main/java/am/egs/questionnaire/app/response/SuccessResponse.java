package am.egs.questionnaire.app.response;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class SuccessResponse<Data> {
    private boolean success;
    private String message;
    private Data data;

    public SuccessResponse(boolean success, String message) {
        this.success = success;
        this.message = message;
    }
}
