package am.egs.questionnaire.app.service;

import am.egs.questionnaire.app.dto.LoginDto;
import am.egs.questionnaire.app.dto.RegisterDto;
import am.egs.questionnaire.app.dto.RegisteredUser;
import am.egs.questionnaire.app.dto.UserWithToken;
import am.egs.questionnaire.app.entity.user.User;

import java.util.List;

public interface UserService {

    void resetCredentials();

    User findById(Long id);

    User findByUsername(String username);

    List<User> findAll();

    RegisteredUser registerNewUser(RegisterDto user);

    UserWithToken login(LoginDto loginDto);
}
