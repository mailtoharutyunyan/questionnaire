package am.egs.questionnaire.app.service;

import am.egs.questionnaire.app.dto.UserWithToken;
import am.egs.questionnaire.app.entity.user.Authority;

import java.util.List;

public interface AuthorityService {

    List<Authority> findById(Long id);

    List<Authority> findByName(String name);


}