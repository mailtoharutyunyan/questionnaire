package am.egs.questionnaire.app.service.impl;

import am.egs.questionnaire.app.dto.LoginDto;
import am.egs.questionnaire.app.dto.RegisterDto;
import am.egs.questionnaire.app.dto.RegisteredUser;
import am.egs.questionnaire.app.dto.UserWithToken;
import am.egs.questionnaire.app.dto.mapper.UserMapper;
import am.egs.questionnaire.app.entity.user.Authority;
import am.egs.questionnaire.app.entity.user.User;
import am.egs.questionnaire.app.exception.ResourceConflictException;
import am.egs.questionnaire.app.repository.UserRepository;
import am.egs.questionnaire.app.security.TokenHelper;
import am.egs.questionnaire.app.security.auth.AuthenticationSuccessHandler;
import am.egs.questionnaire.app.service.AuthorityService;
import am.egs.questionnaire.app.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private final AuthenticationManager authenticationManager;
    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;
    private final AuthorityService authService;
    private final UserMapper userMapper;
    private final TokenHelper tokenHelper;
    private final AuthenticationSuccessHandler authenticationSuccessHandler;


    public void resetCredentials() {
        List<User> users = userRepository.findAll();
        for (User user : users) {
            //TODO test
            user.setPassword(passwordEncoder.encode("123"));
            userRepository.save(user);
        }

    }

    @Override
//    @PreAuthorize("hasRole('USER')")
    public User findByUsername(String username) throws UsernameNotFoundException {
        return userRepository.findByUsername(username);
    }

    @PreAuthorize("hasRole('ADMIN')")
    public User findById(Long id) throws AccessDeniedException {
        return userRepository.getOne(id);
    }

    @PreAuthorize("hasRole('ADMIN')")
    public List<User> findAll() throws AccessDeniedException {
        return userRepository.findAll();
    }

    @Transactional
    @Override
    public RegisteredUser registerNewUser(RegisterDto userRequest) {

        User existUser = findByUsername(userRequest.getUsername());
        if (existUser != null) {
            throw new ResourceConflictException(existUser.getId(), "Username already exists");
        }

        User user = userMapper.fromUserDtoToUser(userRequest);
        List<Authority> auth = authService.findByName("ROLE_USER");
        user.setAuthorities(auth);
        user.setPassword(passwordEncoder.encode(userRequest.getPassword()));
        userRepository.save(user);
        return userMapper.fromUserToRegisteredUser(user);
    }

    @Override
    public UserWithToken login(LoginDto loginDto) {
        authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(loginDto.getUsername(), loginDto.getPassword()));
        String token = tokenHelper.generateToken(
                userRepository.findByUsername(loginDto.getUsername()).getId(),
                loginDto.getUsername(),
                userRepository.findByUsername(loginDto.getUsername()).getAuthorities());

        User userFromDb = userRepository.findByUsername(loginDto.getUsername());
        UserWithToken user = userMapper.fromUserToUserWithToken(userFromDb);
        user.setToken(token);
        return user;
    }
}