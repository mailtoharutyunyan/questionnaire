package am.egs.questionnaire.app.service.impl;

import am.egs.questionnaire.app.entity.user.Authority;
import am.egs.questionnaire.app.entity.user.enums.UserRoleName;
import am.egs.questionnaire.app.repository.AuthorityRepository;
import am.egs.questionnaire.app.service.AuthorityService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class AuthorityServiceImpl implements AuthorityService {

    private final AuthorityRepository authorityRepository;

    @Override
    public List<Authority> findById(Long id) {
        Authority auth = authorityRepository.getOne(id);
        List<Authority> auths = new ArrayList<>();
        auths.add(auth);
        return auths;
    }

    @Override
    public List<Authority> findByName(String name) {
        Authority auth = authorityRepository.findByName(UserRoleName.ROLE_USER);
        List<Authority> auths = new ArrayList<>();
        auths.add(auth);
        return auths;
    }
}