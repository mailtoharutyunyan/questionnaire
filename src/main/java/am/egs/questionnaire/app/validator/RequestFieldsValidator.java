package am.egs.questionnaire.app.validator;


import am.egs.questionnaire.app.error.ErrorFieldResponseDto;
import am.egs.questionnaire.app.error.ValidationError;
import am.egs.questionnaire.app.exception.BaseException;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;

import java.util.ArrayList;
import java.util.List;

@Component
public class RequestFieldsValidator {

    public void validate(Errors errors) {
        if (errors.hasErrors()) {
            List<ErrorFieldResponseDto> errorList = new ArrayList<>();

            errors.getAllErrors().forEach(error -> {
                ErrorFieldResponseDto fieldError = new ErrorFieldResponseDto();
                fieldError.setField(error.getObjectName());
                fieldError.setMessage(error.getDefaultMessage());

                errorList.add(fieldError);
            });
            throw new BaseException(ValidationError.VALIDATION_ERROR, errorList);
        }
    }

}
