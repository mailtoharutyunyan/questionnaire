package am.egs.questionnaire.app.dto;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Getter
@Setter
public class RegisterDto {

    @NotBlank(message = "Username must be not blank")
    @NotNull(message = "Username must be not null")
    private String username;

    @NotBlank(message = "Password must be not blank")
    @NotNull(message = "Password must be not blank")
    private String password;

    @NotBlank(message = "firstName must be not blank")
    @NotNull(message = "firstName must be not blank")
    private String firstName;

    @NotBlank(message = "lastName must be not blank")
    @NotNull(message = "lastName must be not blank")
    private String lastName;

}