package am.egs.questionnaire.app.dto;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Getter
@Setter
public
class PasswordChanger {

    @NotNull(message = "oldPassword Must not be null")
    @NotBlank(message = "oldPassword Must not be blank")
    public String oldPassword;

    @NotNull(message = "oldPassword Must not be null")
    @NotBlank(message = "oldPassword Must not be blank")
    public String newPassword;

}