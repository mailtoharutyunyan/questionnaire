package am.egs.questionnaire.app.dto.mapper;

import am.egs.questionnaire.app.dto.LoginDto;
import am.egs.questionnaire.app.dto.RegisterDto;
import am.egs.questionnaire.app.dto.RegisteredUser;
import am.egs.questionnaire.app.dto.UserWithToken;
import am.egs.questionnaire.app.entity.user.User;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper
public interface UserMapper {

    @Mapping(source = "username", target = "username")
    @Mapping(source = "password", target = "password")
    @Mapping(source = "firstName", target = "firstName")
    @Mapping(source = "lastName", target = "lastName")
    User fromUserDtoToUser(RegisterDto registerDto);

    @Mapping(source = "id", target = "id")
    @Mapping(source = "username", target = "username")
    @Mapping(source = "firstName", target = "firstName")
    RegisteredUser fromUserToRegisteredUser(User user);

    @Mapping(source = "username", target = "username")
    @Mapping(source = "password", target = "password")
    User fromUserLoginToUser(LoginDto loginDto);


    @Mapping(source = "id", target = "id")
    @Mapping(source = "username", target = "username")
    @Mapping(source = "firstName", target = "firstName")
    @Mapping(source = "lastName", target = "lastName")
    UserWithToken fromUserToUserWithToken(User user);

}
