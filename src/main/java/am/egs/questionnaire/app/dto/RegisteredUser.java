package am.egs.questionnaire.app.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RegisteredUser {
    private Long id;
    private String username;
    private String firstName;
}