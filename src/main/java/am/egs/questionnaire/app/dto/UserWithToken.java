package am.egs.questionnaire.app.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserWithToken {
    private Long id;
    private String username;
    private String firstName;
    private String lastName;
    private String token;
}
