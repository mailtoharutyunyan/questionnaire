package am.egs.questionnaire.app.dto;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Getter
@Setter
public class LoginDto {

    @NotBlank(message = "There is an empty username")
    @NotNull(message = "Username is null")
    private String username;
    @NotBlank(message = "There is an empty password")
    @NotNull(message = "Password is null")
    private String password;

}
